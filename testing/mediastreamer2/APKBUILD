# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=mediastreamer2
pkgver=5.0.49
pkgrel=0
pkgdesc="library written in C that allows you to create and run audio and video streams"
url="https://www.linphone.org/technical-corner/mediastreamer2"
arch="all"
license="GPL-2.0-or-later"
options="!check" # no test available
makedepends="cmake gettext-dev ffmpeg-dev ortp-dev
	libxv-dev speex-dev speexdsp-dev libsrtp-dev
	v4l-utils-dev bctoolbox-dev mesa-dev glew-dev
	opus-dev libpcap-dev spandsp-dev tiff-dev libtheora-dev
	alsa-lib-dev python3-dev gsm-dev sqlite-dev libxml2-dev
	jpeg-dev libvpx-dev bzrtp-dev bcg729-dev"
subpackages="$pkgname-dev"
source="https://gitlab.linphone.org/BC/public/mediastreamer2/-/archive/$pkgver/mediastreamer2-$pkgver.tar.gz
	missing-def-o-binary.patch"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_MODULE_PATH=/usr/lib/cmake \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DENABLE_STRICT=NO \
		-DENABLE_ALSA=YES \
		-DENABLE_STATIC=NO \
		-DENABLE_RESAMPLE=YES \
		-DENABLE_JPEG=YES \
		-DENABLE_PCAP=YES \
		-DENABLE_FFMPEG=YES \
		-DENABLE_SPEEX_CODEC=YES \
		-DENABLE_SPEEX_DSP=YES \
		-DENABLE_SHARED=YES \
		-DENABLE_VPX=YES \
		-DENABLE_UNIT_TESTS=NO
	make -C build
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/lib/cmake/Mediastreamer2
	mv "$pkgdir"/usr/share/Mediastreamer2/cmake/* "$subpkgdir"/usr/lib/cmake/Mediastreamer2
	# Remove empty dirs
	rmdir "$pkgdir"/usr/share/Mediastreamer2/cmake
	rmdir "$pkgdir"/usr/share/Mediastreamer2
}

sha512sums="
922b214bed253b9da30a20e0ccc5b23472c656048d9de66d2e20fbaaa032a0be0aacb20c0c7626899f05c147c4a59a5e2296408b7992f22e638e705c12edd99b  mediastreamer2-5.0.49.tar.gz
d0158e03a7552580a9d9f04226c18a31a23d3cc502d52b51a9b083a5c32094ea7349495a7f74316eb727c2478a80b0385e1a28798b5b7d6e85e463485e33a822  missing-def-o-binary.patch
"
