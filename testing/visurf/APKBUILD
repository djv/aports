# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Maxim Karasev <begs@disroot.org>
pkgname=visurf
pkgver=0.0.0_git20211101
_commit=00f0316dfc7e105d4b8be2abb57c636d803c0fa7
_netsurf=3.10
_libcss=accad499aed29acb7bc8fb00bea3d9f2b7f43bd1
pkgrel=0
pkgdesc="power-user frontend for netsurf with vi-inspired keybindings"
url="https://sr.ht/~sircmpwn/visurf/"
arch="all"
license="MIT GPL-2.0-only"
makedepends="
	flex bison gperf wayland-protocols expat-dev curl-dev wayland-dev
	cairo-dev pango-dev libxkbcommon-dev libjpeg-turbo-dev libwebp-dev
	openssl1.1-compat-dev libpng-dev perl
	"
options="!check" # no tests
source="
	https://git.sr.ht/~sircmpwn/visurf/archive/$_commit.tar.gz
	https://download.netsurf-browser.org/netsurf/releases/source-full/netsurf-all-$_netsurf.tar.gz
	http://git.netsurf-browser.org/libcss.git/snapshot/libcss-$_libcss.tar.gz
	"
builddir="$srcdir/netsurf-all-$_netsurf"

build() {
	rm -rf netsurf libcss
	mv "$srcdir"/visurf-$_commit netsurf
	mv "$srcdir"/libcss-$_libcss libcss
	make TARGET=visurf PREFIX=/usr LIBDIR=lib INCLUDEDIR=include
}

package() {
	make TARGET=visurf DESTDIR="$pkgdir" PREFIX=/usr install
	install -Dm755 netsurf/tools/urlfilter "$pkgdir"/usr/bin/urlfilter
}

sha512sums="
53036b584e4a94ae23e8cc710bf59a75894ddc203624aa1e66b2982f63af88a5a4ba590de0beb769579731b031ad4d076706a8e8a55df9606de4e59f25b345af  00f0316dfc7e105d4b8be2abb57c636d803c0fa7.tar.gz
fc4c300eef07c540a7e07f8034db25b7e5fb731b5956029af2220f8638802aa38c055ad54b5683c14501aef9c22fb781b96613c16ae9a6996c3833ceede6f9bf  netsurf-all-3.10.tar.gz
407072ef0a9ddb19a9cd1b7ef7cdf27a32efdbfad81135316b368c2128f8b2eda73baf13700a0c167cc94db5e751a74c506a3366c424272ce55691b99ca79b63  libcss-accad499aed29acb7bc8fb00bea3d9f2b7f43bd1.tar.gz
"
