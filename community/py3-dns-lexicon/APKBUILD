# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-dns-lexicon
pkgver=3.8.2
pkgrel=0
pkgdesc="Manipulate DNS records on various DNS providers in a standardized/agnostic way"
options="!check" # Tests require GitHub tarball which has broken setup.py
url="https://github.com/AnalogJ/lexicon"
arch="noarch"
license="MIT"
depends="
	py3-tldextract
	py3-requests
	py3-yaml
	py3-future
	py3-cryptography
	py3-beautifulsoup4
	"
makedepends="py3-setuptools"
_providerdepends="
	py3-boto3
	py3-pynamecheap
	py3-transip
	py3-xmltodict
	py3-localzone
	py3-softlayer
	py3-zeep
	"
checkdepends="
	py3-filelock
	py3-pytest
	py3-pytest-mock
	py3-requests-file
	py3-vcrpy
	$_providerdepends
	"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/d/dns-lexicon/dns-lexicon-$pkgver.tar.gz"
builddir="$srcdir/dns-lexicon-$pkgver"

build() {
	python3 setup.py build
}

check() {
	pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	rm -rf "$pkgdir"/usr/lib/python3*/site-packages/lexicon/tests
}

sha512sums="
177725069542361c382e4070d3508bbeecb04237e1ca4f2dab3fbf7585871f1d90292c726fc329425d53419b04a5b0ff84abac149fd82c1437fe3fda8370777e  py3-dns-lexicon-3.8.2.tar.gz
"
