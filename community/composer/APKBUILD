# Contributor: Nathan Johnson <nathan@nathanjohnson.info>
# Maintainer: Dave Hall <skwashd@gmail.com>
pkgname=composer
pkgver=2.1.11
pkgrel=0
pkgdesc="Dependency manager for PHP"
url="https://getcomposer.org/"
arch="noarch"
license="MIT"
_php=php8
depends="$_php $_php-phar $_php-curl $_php-iconv $_php-mbstring $_php-openssl $_php-zip"
checkdepends="git"
options="net"
source="$pkgname-$pkgver.phar::https://getcomposer.org/download/$pkgver/composer.phar"

# secfixes:
#   2.1.9-r0:
#     - CVE-2021-41116
#   2.0.13-r0:
#     - CVE-2021-29472

check() {
	cd "$srcdir"
	$_php $pkgname-$pkgver.phar -Vn
	$_php $pkgname-$pkgver.phar -n diagnose || true # fails as pub-keys are missing
}

package() {
	install -m 0755 -D "$srcdir"/$pkgname-$pkgver.phar "$pkgdir"/usr/bin/$pkgname.phar
	printf "#!/bin/sh\n\n/usr/bin/php8 /usr/bin/composer.phar \"\$@\"\n"\
		> "$pkgdir"/usr/bin/$pkgname
	chmod +x "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
b0ade711b3243969fbc156b461b300f7ed51df7de48b51389bc7b3f710a67321822f587618475b9eaa43480ff59c285fdb275a00a0f22cc082120909e8cabd6c  composer-2.1.11.phar
"
