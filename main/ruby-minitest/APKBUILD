# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=ruby-minitest
_gemname=minitest
# Keep version in sync with "Bundled gems" (https://stdgems.org) for the
# packaged Ruby version.
pkgver=5.14.2
pkgrel=0
pkgdesc="Suite of testing facilities supporting TDD, BDD, mocking, and benchmarking for Ruby"
url="https://github.com/seattlerb/minitest"
arch="noarch"
license="MIT"
depends="ruby"
makedepends="ruby-rdoc"
subpackages="$pkgname-doc"
source="https://github.com/seattlerb/minitest/archive/v$pkgver/$_gemname-$pkgver.tar.gz
	https://rubygems.org/downloads/$_gemname-$pkgver.gem
	"
builddir="$srcdir/$_gemname-$pkgver"

prepare() {
	default_prepare

	# Generate gemspec (there's no gemspec in the source).
	gem specification -l --ruby "$srcdir"/$_gemname-$pkgver.gem \
		> "$builddir"/$_gemname.gemspec
}

build() {
	gem build $_gemname.gemspec
}

check() {
	ruby -Ilib -Itest -e "Dir.glob('./test/**/test_*.rb', &method(:require))"
}

package() {
	local gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"
	local geminstdir="$gemdir/gems/$_gemname-$pkgver"

	gem install \
		--local \
		--install-dir "$gemdir" \
		--ignore-dependencies \
		--document ri \
		--verbose \
		$_gemname

	# Remove unnessecary files
	cd "$gemdir"
	rm -rf build_info cache extensions plugins

	cd "$geminstdir"
	rm -rf History.* Manifest.* README.* Rakefile test/
}

doc() {
	pkgdesc="$pkgdesc (ri docs)"

	amove "$(ruby -e 'puts Gem.default_dir')"/doc
}

sha512sums="
7e59cc35abfbd161f688ce58a8194881f3aff00deca9fea47ca00f93ab972990ba08d801e98b2240e35d547ca9010ce39fa2a8fcbf0bca452b80315bf5db888b  minitest-5.14.2.tar.gz
c1cdb87824382233d4ae0570346a6888ecdf0dddbd5fbc9bc136e1c8ed9e1ce57d08d5994f57ba1eee99a38e48a04a10a32288d34ce10ca4ecdfa15587cf602e  minitest-5.14.2.gem
"
